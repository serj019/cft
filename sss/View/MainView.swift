//
//  MainView.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import UIKit

class MainView: UIView {
    
//набор вью элементов
   var labelNotes = UILabel()
    var buttonAdd = UIButton(type: .system)
   var table = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setView()
        setConsraints()
    }
    
// настрока вью элементов
    func setView(){
        buttonAdd.setTitle("+", for: .normal)
        buttonAdd.titleLabel?.font = UIFont(name: "Bradley Hand", size: 25)
        buttonAdd.tintColor = .red
        buttonAdd.contentEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        
        labelNotes.text = "My notes"
        labelNotes.font = UIFont(name: "Bradley Hand", size: 25)
        labelNotes.textColor = .red
        
        //table.separatorStyle = .none
    }
    
//настройка расположения
    func setConsraints(){
        buttonAdd.translatesAutoresizingMaskIntoConstraints = false
        labelNotes.translatesAutoresizingMaskIntoConstraints = false
        table.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(buttonAdd)
        addSubview(labelNotes)
        addSubview(table)
        
        NSLayoutConstraint.activate([
            buttonAdd.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,constant: -10),
            buttonAdd.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            buttonAdd.widthAnchor.constraint(equalToConstant: 25),
            buttonAdd.heightAnchor.constraint(equalToConstant: 25)
        ])
        
        NSLayoutConstraint.activate([
            labelNotes.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            labelNotes.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            table.topAnchor.constraint(equalTo: labelNotes.bottomAnchor,constant: 10),
            table.leadingAnchor.constraint(equalTo: leadingAnchor),
            table.centerXAnchor.constraint(equalTo: centerXAnchor),
            table.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
