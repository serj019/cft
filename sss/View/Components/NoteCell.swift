//
//  NoteCell.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import UIKit

class NoteCell: UITableViewCell {

    static let IdCell = "cell"
    
    var labelNameNote = UILabel()
    var labelInfoNote = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        

        setView()
        setConstraints()
    }
        
  //настройка вью элементов
   func setView(){
       labelNameNote.font = UIFont(name: "Menlo-Bold", size: 25)
       labelNameNote.text = "Hello"
       
       labelInfoNote.font = UIFont(name: "Menlo-Regular", size: 15)
       labelInfoNote.text = "World"

    }
  //настройка расположения
   func setConstraints(){
       labelNameNote.translatesAutoresizingMaskIntoConstraints = false
       labelInfoNote.translatesAutoresizingMaskIntoConstraints = false
       
       addSubview(labelNameNote)
       addSubview(labelInfoNote)
       
       NSLayoutConstraint.activate([
        labelNameNote.topAnchor.constraint(equalTo: topAnchor,constant: 2),
        labelNameNote.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 5),
       ])
       
       NSLayoutConstraint.activate([
        labelInfoNote.topAnchor.constraint(equalTo: labelNameNote.bottomAnchor,constant: 2),
        labelInfoNote.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 5),
       ])
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
