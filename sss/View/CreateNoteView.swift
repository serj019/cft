//
//  CreateNoteView.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import UIKit

class CreateNoteView: UIView {
//набор вью элементов
    var buttonBack = UIButton(type: .system)
    var textFieldNoteName = UITextField()
    var labelInfo = UILabel()
    var textViewNote = UITextView()
    var saveButton = UIButton(type: .system)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setView()
        setConstraints()
    }
    
// настрока вью элементов
    func setView(){
        buttonBack.setTitle("<", for: .normal)
        buttonBack.setTitleColor(.red, for: .normal)
        buttonBack.titleLabel?.font =  UIFont(name: "Bradley Hand", size: 25)
        saveButton.setTitle("Сохранить", for: .normal)

        textFieldNoteName.attributedPlaceholder = NSAttributedString(
            string: "Введите имя заметки",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray.withAlphaComponent(0.6)]
        )
        
        labelInfo.text = "Введите текст заметки:"
        labelInfo.textColor = UIColor.gray.withAlphaComponent(0.6)
        
        textViewNote.backgroundColor = .gray
    }
    
//настройка расположения
    func setConstraints(){
        buttonBack.translatesAutoresizingMaskIntoConstraints = false
        textFieldNoteName.translatesAutoresizingMaskIntoConstraints = false
        labelInfo.translatesAutoresizingMaskIntoConstraints = false
        textViewNote.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(buttonBack)
        addSubview(textFieldNoteName)
        addSubview(labelInfo)
        addSubview(textViewNote)
        addSubview(saveButton)
        
        NSLayoutConstraint.activate([
            saveButton.widthAnchor.constraint(equalToConstant: 100),
            saveButton.heightAnchor.constraint(equalToConstant: 40),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor,
                                               constant: -20)
        ])

        NSLayoutConstraint.activate([
            buttonBack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 0),
            buttonBack.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            buttonBack.widthAnchor.constraint(equalToConstant: 25),
            buttonBack.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        NSLayoutConstraint.activate([
            textFieldNoteName.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 30),
            textFieldNoteName.centerXAnchor.constraint(equalTo: centerXAnchor),
            textFieldNoteName.widthAnchor.constraint(equalToConstant: 200),
            textFieldNoteName.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            labelInfo.topAnchor.constraint(equalTo: textFieldNoteName.bottomAnchor,constant: 20),
            labelInfo.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            textViewNote.topAnchor.constraint(equalTo: labelInfo.bottomAnchor,constant: 10),
            textViewNote.bottomAnchor.constraint(equalTo: saveButton.topAnchor, constant: -10),
            textViewNote.leadingAnchor.constraint(equalTo: leadingAnchor),
            textViewNote.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
