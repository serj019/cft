//
//  CreateNoteViewController.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import UIKit

protocol SaveNoteDelegate {
    func reloadData()
}

class CreateNoteViewController: UIViewController {
    
    var delegate: SaveNoteDelegate?
    var mainView = CreateNoteView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        addAction()
    }
    
    
    func addAction(){
        
        let action = UIAction { _ in
            self.dismiss(animated: true)
        }
        mainView.buttonBack.addAction(action, for: .touchUpInside)
        
        let saveAction = UIAction { _ in
            guard let title = self.mainView.textFieldNoteName.text,
                  let text = self.mainView.textViewNote.text else { return }
            UDService.shared.saveNote(title: title, text: text) {
                self.delegate?.reloadData()
                self.dismiss(animated: true)
            }
        }
        
        mainView.saveButton.addAction(saveAction, for: .touchUpInside)
    }
    
}
