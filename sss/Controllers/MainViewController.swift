//
//  ViewController.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import UIKit

class MainViewController: UIViewController {

    var mainView = MainView()
    var modelNotes: [NoteModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        setTable()
        addAction()
        self.modelNotes = UDService.shared.getNotes()
        mainView.table.reloadData()
    }
    

    func setTable(){
        mainView.table.delegate = self
        mainView.table.dataSource = self
        mainView.table.register(NoteCell.self, forCellReuseIdentifier: NoteCell.IdCell)
    }
    
    func addAction() {
        let action = UIAction { _ in
            let createNoteViewController = CreateNoteViewController()
            createNoteViewController.delegate = self
            createNoteViewController.modalPresentationStyle = .fullScreen
            self.present(createNoteViewController, animated: true)
        }
        mainView.buttonAdd.addAction(action, for: .touchUpInside)
    }
}

extension MainViewController: SaveNoteDelegate {
    func reloadData() {
        self.modelNotes = UDService.shared.getNotes()
        self.mainView.table.reloadData()
    }
}

extension MainViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelNotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NoteCell.IdCell, for:indexPath) as! NoteCell
        let note = modelNotes[indexPath.row]
        cell.labelNameNote.text = note.title
        cell.labelInfoNote.text = note.text
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        55
    }
    
}
