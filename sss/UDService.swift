//
//  UDService.swift
//  sss
//
//  Created by Влад Мади on 26.12.2022.
//

import Foundation

class UDService {
    static let shared = UDService(); private init() { }
    private let ud = UserDefaults.standard
    private let notesKey = "notes"
    
    private var defaultNote = NoteModel(title: "Новая заметка",
                                        text: "Что-то сделать")
    
    func getNotes() -> [NoteModel] {
        guard let representations = ud.value(forKey: notesKey) as? [[String: String]] else {
            return [defaultNote]
        }
        var notes = [NoteModel]()
        for representation in representations {
            if let note = NoteModel(repres: representation) { notes.append(note) }
        }
        return notes
    }
    
    func saveNote(title: String, text: String, completion: @escaping ()->() ) {
        let note = NoteModel(title: title, text: text)
        let repres = note.representation
        if var notes = ud.value(forKey: notesKey) as? [[String: String]] {
            notes.append(repres)
            ud.set(notes, forKey: notesKey)
            completion()
        } else {
            var notes = [defaultNote.representation]
            notes.append(repres)
            ud.set(notes, forKey: notesKey)
            completion()
        }
    }
    
    
}
