//
//  NoteModel.swift
//  sss
//
//  Created by Serj Potapov on 25.12.2022.
//

import Foundation

class NoteModel {
    var title: String
    var text: String
    
    init(title: String, text: String) {
        self.title = title
        self.text = text
    }
    
    init?(repres: [String: String]) {
        guard let title = repres["title"],
              let text = repres["text"] else { return nil }
        self.text = text
        self.title = title
    }
    
    var representation: [String: String] {
        var repres = [String: String]()
        repres["title"] = title
        repres["text"] = text
        return repres
    }
}
